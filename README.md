## PVSS
- [Publicly Veriable Secret Sharing](https://www.ubilab.org/publications/print_versions/pdf/sta96.pdf). Paper introducing PVSS. Markus Stadler, 1996.
- [A Simple Publicly Verifiable Secret Sharing Scheme and its Application to Electronic Voting](https://www.win.tue.nl/~berry/papers/crypto99.pdf). Berry Shoenmakers, 1999.
## Books
